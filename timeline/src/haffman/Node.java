package haffman;

public class Node {
    private int data;
    private int index;
    private Node left;
    private Node right;

    public Node(int data) {
        this.data = data;
    }

    public Node(int data, int index) {
        this.data = data;
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }

    public int getData() {
        return this.data;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getLeft() {
        return this.left;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node getRight() {
        return this.right;
    }
}
