package haffman;


import FileTool.TestTool;

import java.io.*;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class Workers{

    private static LinkedBlockingQueue<Task> blockingQueue = new LinkedBlockingQueue();
    private static Map<Integer, Task> writeTask = new ConcurrentHashMap<>(1000);
    private static final Compress compress = new Compress();

    public static class Producer implements Runnable{
        private int order = 0;

        public void simple() {
            try {
                //读取文件，建树，制表
                String[] huffmanCode = new String[256];
                compress.initTable(huffmanCode);
                byte[] times = compress.countTimes("E:\\youeryuanguanlixitong\\Test.txt");
                Node node = compress.createHuffmanTree(times);
                compress.getHuffmanCode(node, "",huffmanCode);

                FileInputStream fileInputStream = new FileInputStream("E:\\youeryuanguanlixitong\\Test.txt");
                FileOutputStream fileOutputStream = new FileOutputStream("E:\\youeryuanguanlixitong\\Test.zip");

                //将表写入文件
                compress.writeFirstTable(fileOutputStream, huffmanCode);
                //将内容翻译为字节写入文件
                String code ;
                do {
                    code = compress.readAndMake(fileInputStream, huffmanCode);
                    compress.handleContent(code, fileOutputStream);
                }while (code != null);
                fileInputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            Task task ;
            String pathSourch = "E:\\youeryuanguanlixitong\\Test.tx";
            String pathEnd = "E:\\youeryuanguanlixitong\\Test.zip";
            try {
                //读取文件，建树，制表
                String[] huffmanCode = new String[256];
                compress.initTable(huffmanCode);
                byte[] times = compress.countTimes(pathSourch);
                Node node = compress.createHuffmanTree(times);
                compress.getHuffmanCode(node, "",huffmanCode);

                FileInputStream fileInputStream = new FileInputStream(pathSourch);
                FileOutputStream fileOutputStream = new FileOutputStream(pathEnd);

                //将表写入文件
                compress.writeFirstTable(fileOutputStream, huffmanCode);
                //将内容翻译为字节写入文件
                String code ;

                while (true) {
                    code = compress.readAndMake(fileInputStream, huffmanCode);
                    if (code != null) {
                        System.out.println(code);
                        task = new Task();
                        task.setCode(code);
                        task.setPath(pathEnd);
                        task.setOrder(order);
                        blockingQueue.put(task);
                        order++;
                    }else {
                        break;
                    }
                }

                fileInputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void producer() {


        }
    }

    public static class Consumer implements Runnable{

        @Override
        public void run() {
            Task task;
            while (true) {
                try {
                    task = blockingQueue.take();
                    compress.handleContent(task);
                    writeTask.put(task.getOrder(), task);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public static class Writer implements Runnable{
        Task task ;
        int order = 0;
        String path;
        RandomAccessFile randomAccessFile;
        int shift;
        LinkedList<byte[]> linkedList;
        byte[] writerBuffer;
        long fileLength;
        @Override
        public void run() {
            System.out.println("help");
            try {
                for (;;) {
                    task = writeTask.get(order);
                    if (task == null) {
                        for (; ; ) {
                            task = writeTask.get(order);
                            if (task != null) {
                                break;
                            }
                        }
                    }
                    shift = task.getShift();
                    linkedList = task.getLinkedList();
                    randomAccessFile = new RandomAccessFile(task.getPath(), "rw");
                    StringBuffer stringBuffer = new StringBuffer();
                    for (;;) {
                        writerBuffer = linkedList.removeFirst();
                        fileLength = randomAccessFile.length();
                        randomAccessFile.seek(fileLength);
                        if (linkedList.size() == 0) {
                            randomAccessFile.write(writerBuffer,0,shift + 1);
                            stringBuffer = getCode(writerBuffer, stringBuffer,shift + 1);
                            System.out.println(stringBuffer.toString());
                            TestTool.tool();
         //                   TestTool.tool2();
                            break;
                        }
                        randomAccessFile.write(writerBuffer);
                        stringBuffer = getCode(writerBuffer, stringBuffer,0);

                    }
                    order++;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public StringBuffer getCode(byte[] writerBuffer, StringBuffer stringBuffer, int fileLength) {
            int times = writerBuffer.length;
            if (fileLength != 0){
                for (int i = 0; i < fileLength; i++) {
                    stringBuffer.append(writerBuffer[i]&0xff);
                }
            } else {
                for (int i = 0; i < times; i++) {
                    stringBuffer.append(writerBuffer[i]&0xff);
                }
            }
            return stringBuffer;
        }
    }

    public static void main(String[] args) {
        Producer producer = new Producer();
        Consumer consumer = new Consumer();
        Writer writer = new Writer();

        Thread producers = new Thread(producer);
        Thread consumers = new Thread(consumer);
        Thread writers = new Thread(writer);

        producers.setName("producer");
        consumers.setName("consumer");
        writers.setName("writer");

        producers.start();
        consumers.start();
        writers.start();
    }
}
