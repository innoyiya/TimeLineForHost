package haffman;

import java.util.LinkedList;

public class Task {
    private int shift;
    private String path;
    private int order;
    private LinkedList<byte[]> linkedList;
    private String code;

    public void setShift(int shift) {
        this.shift = shift;
    }


    public void setPath(String path) {
        this.path = path;
    }

    public int getShift() {
        return shift;
    }

    public String getPath() {
        return path;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    public void setLinkedList(LinkedList<byte[]> linkedList) {
        this.linkedList = linkedList;
    }

    public LinkedList<byte[]> getLinkedList() {
        return linkedList;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
