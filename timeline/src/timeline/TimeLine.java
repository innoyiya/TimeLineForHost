package timeline;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

/**
 *
 * the class for control thread switch
 * by lianghailun
 * Created by lianghailun on 2017/6/20 0020.
 */
public class TimeLine {
    private class RunningState{
        public List<RunningTask> runningTasks;
        public CountDownLatch beforeLatch; // make sure all threads run with main at the same time
        public CountDownLatch afterLatch;  // this latch is to divide level
        public ConcurrentHashMap<String,Object> map;  // the object to deliver msg in all functions
    }
    private static TimeLine kernel = null;

    private static ExecutorService fixedThreadsPool;

    public static class Builder{
        private List<Object> objects;
        public Builder() {
			// TODO Auto-generated constructor stub
    		objects = new ArrayList<Object>();
		}
    	
    	public Builder addLib(Object object) {
			objects.add(object);
			return this;
		}
    	
    	public synchronized void run(){
            if(kernel==null)
                kernel = new TimeLine();

            kernel.startSteinsGate(objects);
    	}
    }

    public static void shutdown(){
        fixedThreadsPool.shutdown();
    }

    private TimeLine(){
        fixedThreadsPool = Executors.newFixedThreadPool(4);
    }

    /**
     * run the objects with methods
     */
    private  void  startSteinsGate(List<Object> objs){
        List<RunningTask> runningTasks = new ArrayList<>();
        for (Object obj : objs) {
            runningTasks.addAll(trackTask(obj, obj.getClass()));//parse each obj thread
        }
        startRunInPool(runningTasks);

    }


    private void startRunInPool(List<RunningTask> runningTasks) {
        RunningState state = new RunningState();
        state.runningTasks = runningTasks;
        state.map = new ConcurrentHashMap<String,Object>();
        System.out.println("before");
        fixedThreadsPool.execute(new MainRunnable(state));
        System.out.println("after");
    }

    private class MainRunnable implements Runnable{
        private RunningState state;

        public MainRunnable( RunningState state) {
            this.state = state;
        }

        @Override
        public void run() {
            long start = System.currentTimeMillis();
            try {
                for (final RunningTask task : state.runningTasks) {
                    state.beforeLatch = new CountDownLatch(1);//set the latch make sure all run in same time
                    int sum = 0;
                    for(Integer i:task.copys){
                        sum += i;
                    }
                    state.afterLatch = new CountDownLatch(sum);// count down all thread in a level
                    for(int i=0;i<task.methods.size();i++){
                        for(int j=0;j<task.copys.get(i);j++){
                            fixedThreadsPool.execute(new MyRunnable(task.obj, task.methods.get(i), j,state));
                        }
                    }
                    state.beforeLatch.countDown();
                    try {
                        state.afterLatch.await();// wait for a level to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            System.out.println(System.currentTimeMillis() - start + "ms");
        }
    }

    private class MyRunnable implements Runnable{
        private Object object;
        private Method method;
        private int copy;
        private RunningState state ;

        public MyRunnable(Object object, Method method, int copy,RunningState state ) {
            this.object = object;
            this.method = method;
            this.copy = copy;
            this.state = state;
        }
        @Override
        public void run() {
            try {
                state.beforeLatch.await();// wait for mainThread
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                RunningMsg msg = new RunningMsg();
                msg.copy = copy;
                msg.maps = state.map;
                method.invoke(object,msg);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            state.afterLatch.countDown();// countdown for a level
        }
    }

    /**
     * bind method in a object
     * @param obj
     * @param cl
     */
    public  List<RunningTask> trackTask( Object obj,Class<?> cl) {
        List<RunningTask> runningTasks = new ArrayList<>();
        for (Method m : cl.getDeclaredMethods()) {
            TLine tick = m.getAnnotation(TLine.class);
            if (tick != null) {
                System.out.println("parsing Level:" + tick.level() + "copy"
                        + tick.copy());
                boolean isAready = false;
                for(RunningTask runningTask : runningTasks){
                    if(runningTask.level == tick.level()){
                        isAready = true;
                        runningTask.methods.add(m);
                        runningTask.copys.add(tick.copy());
                        runningTask.obj = obj;
                    }
                }
                if(isAready){
                    continue;
                }else{
                    RunningTask runningTask = new RunningTask();
                    runningTask.level = tick.level();
                    runningTask.copys = new ArrayList<Integer>();
                    runningTask.methods = new ArrayList<Method>();
                    runningTask.methods.add(m);
                    runningTask.copys.add(tick.copy());
                    runningTask.obj = obj;
                    runningTasks.add(runningTask);
                }
            }

        }
        Collections.sort(runningTasks,new LevelComparator());
        return runningTasks;
    }

    /**
     * sort for level
     */
    private class  LevelComparator implements Comparator{


        @Override
        public int compare(Object o1, Object o2) {
            RunningTask t1= (RunningTask) o1;
            RunningTask t2= (RunningTask) o2;
            return t1.level-t2.level;
        }
    }

    /**
     * model for running
     */
    private class RunningTask{
        public int level = 0;
        public Object obj;
        public List<Method> methods;
        public List<Integer> copys;
    }
    
    /**
     * message for running
     * @author Administrator
     *
     */
    public static class RunningMsg{
    	public int copy;
    	public ConcurrentHashMap<String, Object> maps;
    }

    /**
     * Created by Administrator on 2017/6/17 0017.
     */
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TLine {
        int level() default 1;
        int copy() default 1;
    }
}

