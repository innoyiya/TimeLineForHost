package timeline;

import timeline.TimeLine.RunningMsg;
import timeline.TimeLine.TLine;

import java.io.Serializable;

public class LibOne implements Serializable{
	
	@TLine(level = 2 ,copy = 2)
	public void method1(RunningMsg msg){
		System.out.println("level 2--A copy = "+msg.copy+" :"+Thread.currentThread());
	}
	
	
	@TLine(level = 3 ,copy = 3)
	public void method2(RunningMsg msg){
		System.out.println("level 3--A copy = "+msg.copy+" :"+Thread.currentThread());
	}
	
	@TLine(level = 3 ,copy = 2)
	public void method3(RunningMsg msg){
		System.out.println("level 3--B copy = "+msg.copy+" :"+Thread.currentThread());
	}
}
