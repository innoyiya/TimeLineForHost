package timeline;

public class Bench {
	public static void main(String[] args) {
		/*
		new TimeLine.Builder()
		.addLib(new LibOne())
		.addLib(new LibTwo())
		.run();
*/

		/**
		 new TimeLine.Builder()
		 .addLib(new Bench()).run();
		 *
		 */
		/**
		 * 0代表直接创建线程执行，非0代表使用线程池
		 */
		TimeLine.Builder builder = new TimeLine.Builder();
		int i = 0;
		for (; i < 1000; i++) {
			builder.addLib(new Bench());
		}
		builder.run();
	}



	@TimeLine.TLine(copy = 4)
	public void count2(TimeLine.RunningMsg msg){
		long bstart = System.currentTimeMillis();
		int sum = 0;
		int i = msg.copy;
		for(; i <= 100; i+=4){
			sum+=i;
	//		System.out.println(Thread.currentThread().getName()+" " +i);
		}
		long bend = System.currentTimeMillis();
	//	System.out.println("copy:"+msg.copy+" "+(bend-bstart) + " " + msg.maps.hashCode() + " size: " +msg.maps.size());
	//	System.out.println("sum " + sum);
		msg.maps.put("sum"+msg.copy,sum);

	}

	@TimeLine.TLine(level = 2)
	public void count1(TimeLine.RunningMsg msg){

		Integer integer0 = (Integer) msg.maps.get("sum0");
		Integer integer1 = (Integer) msg.maps.get("sum1");
		Integer integer2 = (Integer) msg.maps.get("sum2");
		Integer integer3 = (Integer) msg.maps.get("sum3");
		System.out.println(integer0);
		System.out.println(integer1);
		System.out.println(integer2);
		System.out.println(integer3);
		System.out.println(integer0+integer2+integer3+integer1);
	}

}
