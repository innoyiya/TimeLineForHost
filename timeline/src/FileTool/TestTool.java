package FileTool;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestTool {

    private static StringBuffer addZeroForString(int last, StringBuffer code) {

        switch (last) {
            case 7 :
                code.append("0000000");
                break;
            case 6 :
                code.append("000000");
                break;
            case 5 :
                code.append("00000");
                break;
            case 4 :
                code.append("0000");
                break;
            case 3 :
                code.append("000");
                break;
            case 2 :
                code.append("00");
                break;
            case 1 :
                code.append("0");
                break;
        }
        return code;
    }
    private static int bit2byte(String bString) {
        int result = 0;
        for (int i = bString.length() - 1, j = 0; i >= 0; i--, j++) {
            result += (Byte.parseByte(bString.charAt(i) + "") << j);
        }

        return result;
    }

    public static StringBuffer tool() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("E:\\youeryuanguanlixitong\\Test.txt");

        StringBuffer stringBuffer = new StringBuffer();
        int value = fileInputStream.read();
        while (value != -1) {

            if (value == 97) {
                stringBuffer.append("00");
            } else if (value == 98) {
                stringBuffer.append("10");
            } else if (value == 99) {
                stringBuffer.append("111");
            }  else if (value == 100) {
                stringBuffer.append("01");
            } else if (value == 10) {
                stringBuffer.append("1101");
            } else if (value == 13) {
                stringBuffer.append("1100");
            }
            value = fileInputStream.read();
        }
        System.out.println(stringBuffer.toString());
        StringBuffer result = new StringBuffer();
        int codeLengthTimes = stringBuffer.length() / 8;
        System.out.println("real " + codeLengthTimes);
        for (int i = 0; i < codeLengthTimes; i++) {
            result.append(bit2byte(stringBuffer.substring(i << 3, (i + 1) << 3)));
        }
        int leftLength = stringBuffer.length() & 7;
        if (leftLength == 0) {
            result.append(0);
            return result;
        }
        int lastIndex = 8 - leftLength;

        stringBuffer = addZeroForString(lastIndex, stringBuffer);

        result.append(bit2byte(stringBuffer.substring(stringBuffer.length() - 8, stringBuffer.length())));
        System.out.println(result.toString());
        return result;
    }

    public static StringBuffer tool2() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("E:\\youeryuanguanlixitong\\t2.txt");

        StringBuffer stringBuffer = new StringBuffer();
        int value = fileInputStream.read();
        while (value != -1) {

            if (value == 97) {
                stringBuffer.append("00");
            } else if (value == 98) {
                stringBuffer.append("10");
            } else if (value == 99) {
                stringBuffer.append("111");
            }  else if (value == 100) {
                stringBuffer.append("01");
            } else if (value == 10) {
                stringBuffer.append("1101");
            } else if (value == 13) {
                stringBuffer.append("1100");
            }
            value = fileInputStream.read();
        }
        System.out.println(stringBuffer.toString());
        StringBuffer result = new StringBuffer();
        int codeLengthTimes = stringBuffer.length() / 8;
        for (int i = 0; i < codeLengthTimes; i++) {
            result.append(bit2byte(stringBuffer.substring(i << 3, (i + 1) << 3)));
        }
        int leftLength = stringBuffer.length() & 7;
        if (leftLength == 0) {
            result.append(0);
            return result;
        }
        int lastIndex = 8 - leftLength;
        System.out.println(lastIndex);
        stringBuffer = addZeroForString(lastIndex, stringBuffer);

        result.append(bit2byte(stringBuffer.substring(stringBuffer.length() - 8, stringBuffer.length())));
        return result;
    }

    public static StringBuffer getFileValue(String path) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(path);
        int index = fileInputStream.read();
        StringBuffer date = new StringBuffer();
        while (index != -1) {
            date.append(index);
            index = fileInputStream.read();
        }
        date = date.delete(0,264 );
        System.out.println(date.toString());
        return date;
    }
    public static void main(String[] args) throws IOException {
        //System.out.println(TestTool.getFileValue("E:\\youeryuanguanlixitong\\Test.zip").equals(TestTool.tool()));
        TestTool.tool();
        TestTool.tool2();

    }
}
